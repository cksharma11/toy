const printBoard = board => {
  console.log("_", board[0], "_|_", board[1], "_|_", board[2], "_");
  console.log("_", board[3], "_|_", board[4], "_|_", board[5], "_");
  console.log(" ", board[6], " | ", board[7], " | ", board[8], " ");
};

const createPlayer = (name, symbol) => {
  return { name, moves: [], symbol };
};

const makeMove = (moves, move) => {
  const update = moves.slice();
  update.push(move);
  return update;
};

const getNextPlayer = current => {
  return 1 - current;
};

const hasWon = (moves, winningCombinations) => {
  return winningCombinations.some(combination =>
    combination.every(move => moves.includes(move))
  );
};

const initGame = (first, second) => {
  const winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 4, 8],
    [2, 4, 6],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8]
  ];
  const board = new Array(9).fill(" ");
  const players = [createPlayer(first, "X"), createPlayer(second, "O")];
  return { players, current: 0, board, winningCombinations };
};

const moves = (players, current) => players[current].moves;

const getNewMove = board => {
  let move = Math.floor(Math.random() * 9);
  while (board[move] !== " ") {
    move = Math.floor(Math.random() * 9);
  }
  return move;
};

const draw = board => board.every(m => m !== " ");

const runner = ({ players, current, board, winningCombinations }) => {
  while (
    !hasWon(moves(players, current), winningCombinations) &&
    !draw(board)
  ) {
    current = getNextPlayer(current);
    const move = getNewMove(board);
    board[move] = players[current].symbol;
    players[current].moves = makeMove(moves(players, current), move);
  }
  printBoard(board);
};

const engine = {
  create: (first, second) => initGame(first, second),
  run: game => runner(game)
};

const manager = () => {
  const game = engine.create("chandan", "ranjan");
  engine.run(game);
};

manager();
